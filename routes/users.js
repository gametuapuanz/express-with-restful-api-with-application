const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const cors = require('cors');
let users = require('../users.json');
let server = express();
server.use(bodyParser.json());
server.use(morgan('dev'));
server.use(cors());



server.get('/', function (req, res, next) {
  res.send("index.html")
})
server.get('/user/login', function (req, res, next) {
  res.send("result.html")
})


module.exports = server;